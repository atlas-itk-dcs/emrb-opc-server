/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <thread>
#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>
#include <DRoot.h>
#include <DEMRBBoard.h>
#include "ASEMRBBoard.h"
#include <DChannel.h>
#include "ASChannel.h"
#include "emrb/emrb.h"

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    emrb* ee= new emrb();
    ee->Init();
    //    ee->Calibrate();

    
    
    while(ShutDownFlag() == 0)
    {

      for(Device::DEMRBBoard *brd : Device::DRoot::getInstance()->emrbboards()){
	brd=brd;
	for(Device::DChannel *chn : brd->channels()){
	  if(ShutDownFlag()) break;
	  uint32_t chan = chn->getAddressSpaceLink()->getChannelNumber();
	  
	  std::cout<<"Selecting Chan: "<<chan<<std::endl;
 	  //ee->SelectChannelByName(chn->getName());

	  ee->SelectChannel(chan);
	  //std::cout<<"Selected Chan: "<<chan<<std::endl;

	  auto now = std::chrono::system_clock::now();
	  // Convert point in time to epoch time
	  auto now_time_t = std::chrono::system_clock::to_time_t(now);

	  uint32_t adc=ee->ReadRawValue();
	  double volts=ee->CalibrateV(adc);
	  double celsius=ee->CalibrateT(volts);
	  
	  chn->getAddressSpaceLink()->setDateAndTime(static_cast<uint64_t>(now_time_t), OpcUa_Good);
	  chn->getAddressSpaceLink()->setADCCalibrationFactor(0.00003619778, OpcUa_Good);
	  chn->getAddressSpaceLink()->setEnable(true, OpcUa_Good);
	  chn->getAddressSpaceLink()->setADCValue(adc, OpcUa_Good);
	  chn->getAddressSpaceLink()->setVoltage(volts, OpcUa_Good);
	  chn->getAddressSpaceLink()->setTemperature(celsius, OpcUa_Good);
	  
	}
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    
    delete ee;
    
    printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
  LOG(Log::INF) << "Initializing Quasar server.";
}

void QuasarServer::shutdown()
{
  LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
  BaseQuasarServer::initializeLogIt();
  LOG(Log::INF) << "Logging initialized.";
}
