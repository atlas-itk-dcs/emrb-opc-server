#include "emrb/SPIControl.h"
#include "emrb/MultControl.h"
#include "emrb/CS5523.h"
#include <linux/spi/spidev.h>

#include <sstream>
#include <iomanip>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <iostream>
#include <math.h>
#include <chrono>
#include <thread>
#include <stdexcept>
#include <utility>
#include <errno.h>
#include <stdio.h>
#include <string.h>

using namespace std;

#define SPI_DLEN_OFFSET          0x0C

SPIControl * SPIControl::m_me=0;

SPIControl::SPIControl(){
  m_verbose=0;
  m_fd=-1;
  m_lock.unlock();

  m_device_name="/dev/spidev0.0";
  m_speed=1000; //Speed in HZ
  m_maxTries=4;

  m_bits_per_word_wr=8; //This is permenant
  m_bits_per_word_rd=8; 
  
  //Sampling Rates
  // 000 15 SpS
  // 001 30 Sps
  // 010 61.6 Sps
  // 011 84.5 Sps
  // 100 101.1
  // 101 1.88
  // 110 3.76
  // 111 7.51 

  //The input crystall here is 32.768 kHz
  m_sampling_rate=0x2; 


  //----- SET SPI MODE -----
  //SPI_MODE_0 (0,0) 	CPOL = 0, CPHA = 0, Clock idle low, data is clocked in on rising edge, output data (change) on falling edge
  //SPI_MODE_1 (0,1) 	CPOL = 0, CPHA = 1, Clock idle low, data is clocked in on falling edge, output data (change) on rising edge
  //SPI_MODE_2 (1,0) 	CPOL = 1, CPHA = 0, Clock idle high, data is clocked in on falling edge, output data (change) on rising edge
  //SPI_MODE_3 (1,1) 	CPOL = 1, CPHA = 1, Clock idle high, data is clocked in on rising, edge output data (change) on falling edge
  
  m_spi_mode=0; // There is a lot of spi mode objects

  // const uint32_t ADC_CS = 8; //GPIO pins that enables Serial data
  // const uint32_t ADC_SDI = 10; //GPIO pins that have serial data into the ADC
  // const uint32_t ADC_SDO = 9; //GPIO pins that SDO data out that  will flow data to us
  // const uint32_t ADC_SCLK = 11; //GPIO pins that provide the CLK to ADC
  
}

SPIControl::~SPIControl(){
  Close();
}

SPIControl * SPIControl::GetInstance(){
  if(m_me==0){ m_me = new SPIControl();}
  return m_me;
}

bool SPIControl::GetVerbose(){
  return m_verbose;
}

void SPIControl::SetVerbose(bool enable){
  m_verbose=enable;
}


bool SPIControl::Open(){
  m_lock.lock();
  m_fd=open(m_device_name.c_str(), O_RDWR);
  if(m_fd<0){cout << "Failed to open SPI bus: "<<m_device_name<< endl; return false;}

  int ret=0;

  // //Set write speed
  ret = ioctl(m_fd, SPI_IOC_WR_MAX_SPEED_HZ, &m_speed);
  if (ret == -1){
    cout<<" Failed to set the write speed to "<<m_speed<<"Hz"<<endl; return false;
  }
  //Set read speed 
  ret = ioctl(m_fd, SPI_IOC_RD_MAX_SPEED_HZ, &m_speed);
  if (ret == -1){
    cout<<" Failed to set the read speed to "<<m_speed<<"Hz"<<endl; return false;
  }

  ret = ioctl(m_fd, SPI_IOC_WR_BITS_PER_WORD, &m_bits_per_word_wr);
  if (ret == -1){
    cout<<" Failed to set the Write bits per word: "<<m_bits_per_word_wr<<endl; return false;
  }

  ret = ioctl(m_fd, SPI_IOC_RD_BITS_PER_WORD, &m_bits_per_word_rd);
  if (ret == -1){
    cout<<" Failed to set the Read bits per word: "<<m_bits_per_word_rd<<endl; return false;
  }
  
  //Setting SPI Mode
  ret = ioctl(m_fd, SPI_IOC_WR_MODE, &m_spi_mode);
  if (ret == -1){
    cout<<" Failed to set the WR Mode: "<<m_spi_mode<<endl; return false;
  }
  ret = ioctl(m_fd, SPI_IOC_RD_MODE, &m_spi_mode);
  if (ret == -1){
    cout<<" Failed to set the RD Mode: "<<m_spi_mode<<endl; return false;
  }
    
  m_lock.unlock();
  return true;
}






bool SPIControl::IsOpen(){
  if(m_verbose){cout << "File descriptor is: " << m_fd << endl;}
  return (m_fd>0?true:false);
}

void SPIControl::Close(){
  if(m_fd<=0){return;}
  close(m_fd);
  m_fd=-1;
}



unsigned long SPIControl::WriteAndRead(uint8_t * data, unsigned len, bool KeepHigh=0){
  m_lock.lock();
  //cout<<"Testing stuff "<<data.size()<<endl;

  struct spi_ioc_transfer tr = {};

  // cout<<endl<<"Sending Command 0x";
  // for (unsigned ind=0;ind<len;ind++)
  //   cout<<hex<<int(data[ind]>>4)<<int(data[ind]&0xF)<<" "<<dec;
  // cout<<endl;



  
  uint8_t * rx_data;
  rx_data = new uint8_t[4] {0x11,0x22,0x33,0x44};
  //cout<<"Pointer "<<uint64_t(rx_data)<<" "<<uint32_t(rx_data[0])<<" "<<uint32_t(rx_data[1])<<" ???? ";
  
  tr.tx_buf = (unsigned long)data;
  tr.rx_buf = (unsigned long)rx_data;
  tr.len = len;
  tr.delay_usecs = 5;
  tr.speed_hz = m_speed;
  tr.bits_per_word =m_bits_per_word_wr; //Try 0
  tr.cs_change=KeepHigh; // Keep enable high low as this is last transfer
  tr.pad=0; // Keep enable high low as this is last transfer
  tr.tx_nbits=len*8;
  tr.rx_nbits=24;

  // cout<<"CMD Data :";
  // for(unsigned ind=0;ind<tr.len;ind++){
  //   cout<< int ( data[ind])<<" ";
  // }
  // cout<<endl;
  int check = -1;
  unsigned tryCount=0;
  
  while(check<0){
    check=ioctl(m_fd, SPI_IOC_MESSAGE(1), &tr);
    if(check==-1){

      cout<<dec<<"Check message returned: "<<check<<endl;
      cout<<"Error: "<<strerror(errno)<<endl;
      cout<<" tx: "<<tr.tx_buf
		 <<" rx: "<<tr.rx_buf
		 <<" len: "<<len
		 <<" delay: "<<tr.delay_usecs
		 <<" speed: "<<tr.speed_hz
		 <<" bpw: "<<tr.bits_per_word
		 <<" cs: "<<tr.cs_change
		 <<" pad: "<<tr.pad<<endl;

    }
    tryCount++;
  
    if(check<0) this_thread::sleep_for(chrono::milliseconds(10));
    if(tryCount > m_maxTries){
      break;
    }
  }

  // cout<<"Check size: "<<check<<" "<<len<<endl;
  
  m_lock.unlock();
  uint32_t Return = ((uint32_t(rx_data[1])<<16) + (uint32_t(rx_data[2])<<8) + (uint32_t(rx_data[3])));
  delete [] rx_data;
  return Return;
}

unsigned long SPIControl::WriteAndRead(uint8_t data){
  vector<uint8_t> vdata={data};
  return WriteAndRead(vdata.data(),vdata.size());
}

void SPIControl::ADC_Init(){

  this_thread::sleep_for(chrono::milliseconds(500));

  //Change the ADC to command mode

  //cout<<"Reset Set: "<<hex<<WriteAndRead(m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config))<<dec<<endl;
  vector<uint8_t> CMD;
  uint64_t Response;
  //Initialize command is 15 bytes of all 1
  for(uint8_t ind=0;ind<15;ind++)
    CMD.push_back(0xFF); 

  //followed by 1111 110' (The coumentation is not clear here is it's 1100 or 1110
  CMD.push_back(0xFE);
  //CMD.push_back(0xFC);
  Response = WriteAndRead(CMD.data(),CMD.size());
  cout<<"Just sent 0xFF..FC -> "<<hex<<Response<<dec<<endl;
  this_thread::sleep_for(chrono::milliseconds(1000));

  // CMD=m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config);
  // cout<<"Attempting read config register: "<<hex<<WriteAndRead(CMD.data(),CMD.size())<<dec<<endl;


  
  //After command mode, first command should be system reset.
  //This is accomplished by writing a logic 1 to the RS (Reset System) bit in the configuration register.
  //this_thread::sleep_for(chrono::milliseconds(10000));
  m_ADC.SetField(m_ADC.RESET_SYSTEM,1);
  CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);
  Response = WriteAndRead(CMD.data(),CMD.size());
  cout<<"just set the Reset system bit to 1 -> "<<hex<<Response<<dec<<endl;
 
  
  this_thread::sleep_for(chrono::milliseconds(1000));
  m_ADC.SetField(m_ADC.RESET_SYSTEM,0);
  CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);
  Response = WriteAndRead(CMD.data(),CMD.size());
  cout<<"just set the Reset system bit to 0 -> "<<hex<<Response<<dec<<endl;

  this_thread::sleep_for(chrono::milliseconds(100));
  CMD=m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config);
  Response=WriteAndRead(CMD.data(),CMD.size());
  cout<<"Attempting read config register: "<<hex<<Response<<dec<<endl;
  
  //Time to setup the running config
  
  //For doing CSR configure we will always edit the full depth for ease of use. 
  m_ADC.SetField(m_ADC.DEPTH_POINTER,0x7);
  // Don't loop, only read the requested
  m_ADC.SetField(m_ADC.MULTIPLE_CONVERSION,0x0);
  m_ADC.SetField(m_ADC.LOOP,0x0);
  CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);
  Response = WriteAndRead(CMD.data(),CMD.size());

  cout<<"Setting config registers after init "<<hex<<Response<<dec<<endl;
  
  CMD=m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config);
  Response=WriteAndRead(CMD.data(),CMD.size());
  cout<<"Attempting read config register: "<<hex<<Response<<dec<<endl;


  //cout<<"Reading out the ChanSetup Register:"<<hex<<ADC_Read_ChanSetup()<<dec<<endl;
  
  for(uint chn=0;chn<4;chn++){
    // ADC_Configure_CSR( chn+1, 0x0, m_sampling_rate, chn, 0);
    // ADC_Configure_CSR( chn+5, 0x0, m_sampling_rate, chn, 0);
    ADC_Configure_CSR( chn+1, 0x5, m_sampling_rate, chn, 0);
    ADC_Configure_CSR( chn+5, 0x5, m_sampling_rate, chn, 0);
  }  
  
}

void SPIControl::ADC_Configure_CSR(uint8_t CSR, uint8_t Gain, uint8_t WordRate, uint8_t Chn, bool Send){

  if(not (CSR>=1 and CSR<=8) ) {
    cout<<"CSR value is out of range, it must be between 1 and 8"<<endl;
    return;
  }
  
  m_ADC.SetField(m_ADC.UNIPOLAR_MODE[(CSR-1)],1); //Unipolar is forced in this card
  m_ADC.SetField(m_ADC.LATCH_OUTPUT[(CSR-1)],00); //Latch Mode are fixed
  m_ADC.SetField(m_ADC.GAIN_BITS[(CSR-1)],Gain);
  m_ADC.SetField(m_ADC.WORD_RATE[(CSR-1)],WordRate);
  m_ADC.SetField(m_ADC.CHANNEL_SELECT[(CSR-1)],Chn);

  if(Send){
    vector<uint8_t> CMD = m_ADC.GetCmd_WriteReg(m_ADC.m_reg_chanSetup);
    WriteAndRead(CMD.data(),CMD.size());
  }
}


void SPIControl::ADC_Setup_Read_Single(){

  m_ADC.SetField(m_ADC.MULTIPLE_CONVERSION,0x0);
  m_ADC.SetField(m_ADC.LOOP,0x0);
  vector<uint8_t> CMD = m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);
  WriteAndRead(CMD.data(),CMD.size());
}


unsigned long SPIControl::ADC_Read_Single(uint8_t CSR){
  vector<uint8_t> CMD=m_ADC.GetCmd_PerfConv(CSR);
  WriteAndRead(CMD.data(),CMD.size() , 1 );
  this_thread::sleep_for(chrono::milliseconds(100));
  CMD.clear();
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  return WriteAndRead(CMD.data(),CMD.size());
}

void SPIControl::ADC_Setup_Read_Mult(){
  m_ADC.SetField(m_ADC.MULTIPLE_CONVERSION,0x1);
  m_ADC.SetField(m_ADC.LOOP,0x0);
  vector<uint8_t> CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_config);
  WriteAndRead(CMD.data(),CMD.size()  ); 

}

unsigned long SPIControl::ADC_Read_Mult(){
  vector<uint8_t> CMD=m_ADC.GetCmd_PerfConv(0);
  return WriteAndRead(CMD.data(),CMD.size()    ); //The CSR number doesn't matter here because they will
}

unsigned long SPIControl::ADC_Read_Config_Register(){
  vector<uint8_t> CMD= m_ADC.GetCmd_ReadReg(m_ADC.m_reg_config);
  return WriteAndRead(CMD.data(),CMD.size()  ); //The CSR number doesn't matter here because they will be all readout
}

void SPIControl::ADC_Set_Gain(uint8_t chn, uint32_t Gain){
  m_ADC.SetField(m_ADC.CH_GAIN[chn],Gain);

  vector<uint8_t> CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_gain[chn]);
  WriteAndRead(CMD.data(),CMD.size()  );
  
}
void SPIControl::ADC_Set_Sign(uint8_t chn, uint8_t sign){
  m_ADC.SetField(m_ADC.CH_SIGN[chn],sign);

  vector<uint8_t> CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_gain[chn]);
  WriteAndRead(CMD.data(),CMD.size()  );
  
}
void SPIControl::ADC_Set_Offset(uint8_t chn, uint32_t Offset){
  m_ADC.SetField(m_ADC.CH_OFFSET[chn],Offset);

  vector<uint8_t> CMD=m_ADC.GetCmd_WriteReg(m_ADC.m_reg_offset[chn]);
  WriteAndRead(CMD.data(),CMD.size()  );
}  

pair<uint8_t, uint8_t> SPIControl::ADC_Read_Pin(uint8_t pinValue) {
    uint8_t lowBits = 0, highBits = 0, pinNumber = 0, EnInd = 0, ADCChn = 0, correctedPinValue = 0;

    try {
      if (not (pinValue >= 1 and pinValue <= 52)){
	throw invalid_argument("Invalid pin value");
      } else {
	
	//uint8_t correctedPinValue = pinValue -= 1;
	if (pinValue <= 26){
	  pinNumber = pinValue;
	  correctedPinValue = pinValue -= 1;
	  uint8_t correctionFactor = (correctedPinValue/4);
	  correctedPinValue -= (correctionFactor*2);
	  if (correctionFactor%2 != 0) correctedPinValue += 14;
	}
	
	if (pinValue > 26){
	  pinNumber = pinValue - 26;
	  correctedPinValue = pinValue -= 27;
	  uint8_t correctionFactor = (correctedPinValue/4);
	  correctedPinValue -= (correctionFactor*2);
	  if (correctionFactor%2 != 0) correctedPinValue += 14;
	  correctedPinValue += 32;
	}
	
	// Extract 4 LSB
	lowBits = correctedPinValue & 0x0F;
	
	// Attribute these bits to EnInd
	EnInd = lowBits;
	
	// Extract 2 MSB
	highBits = (correctedPinValue >> 4) & 0x03;
	
	// Attribute these bits to EnInd
	ADCChn = highBits;
	
	// Call SetEnable with ADCChn & EnInd
	// EnCon->SetEnable(ADCChn);
	// EnCon->SetEnable(EnInd);
	
	if (ADCChn >= 0 and ADCChn <= 1) {
	  cout << "SCB_A_" << static_cast<int>(pinNumber); 
	  // pinNumber = pinValue; //EnInd + (ADCChn); // * 4 + (EnInd / 4) * 4;
	} else if (ADCChn >= 2 and ADCChn <= 3) {
	  cout << "SCB_B_" << static_cast<int>(pinNumber); 
	  //pinNumber = pinValue -26;  //EnInd + (ADCChn); //(ADCChn - 2) * 4 + (EnInd / 4) * 4;
	}
	// cout << static_cast<int>(pinNumber + 1);
      }
    } catch (const invalid_argument& e) {
      cerr << "Error: " << e.what() << endl;
      return make_pair(static_cast<uint8_t>(-1), static_cast<uint8_t>(-1)); // Return a pair indicating an error
    }
    
    return make_pair(EnInd, ADCChn);
}


