#include "emrb/MultControl.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <sys/mman.h>
using namespace std;

MultControl * MultControl::m_me=0;

MultControl::MultControl(){
  m_verbose=0;
  m_fd=-1;
  m_lock.unlock();

  //Initialise RPI
  uint32_t model=2711;
  
  m_pi_base=0x20000000;
  if      (model==2835){ m_pi_base=0x3F000000;} 
  else if (model==2711){ m_pi_base=0xFE000000;}

  m_gpio_base = m_pi_base + 0x00200000;

  
  m_gpio_to_set = {
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  };

  m_gpio_to_clr = {
    10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
    11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
  };


  m_gpio_to_lev =
    {
     13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
     14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
    };

  m_gpio_to_sel =
    {
     0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,
     3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,
    };

  m_gpio_to_shift =
    {
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
     0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,0,3,6,9,12,15,18,21,24,27,
    } ;
		    
  Open();

  //Enable the GPIO pins in the output mode 
  for(uint8_t ind=0;ind<4;ind++)
    SetMode(ENABLES[ind],MODE_OUTPUT);
  
}

MultControl::~MultControl(){
  Close();
}

MultControl * MultControl::GetInstance(){
  if(m_me==0){ m_me = new MultControl();}
  return m_me;
}

bool MultControl::Open(){
  m_lock.lock();
  m_fd=open("/dev/gpiomem", O_RDWR | O_SYNC | O_CLOEXEC);
  if(m_fd<0){cout << "Failed to open: /dev/gpiomem" << endl; return false;}
  m_gpio = (uint32_t *)mmap(0, (4*1024), PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, m_gpio_base);
  if(m_gpio==MAP_FAILED) {cout << "Cannot mamp /dev/gpiomem" << endl;}
  m_lock.unlock();
  return true;
}

void MultControl::Close(){

}

void MultControl::Write(uint32_t gpio, bool enable){
  if(m_verbose){cout << "gpio: " << gpio << endl;}
  if(gpio<0) return;
  if(enable){
    *(m_gpio + m_gpio_to_set[gpio]) = 1 << gpio;
  }else{
    *(m_gpio + m_gpio_to_clr[gpio]) = 1 << gpio;
  }
}

bool MultControl::Read(uint32_t gpio){
  if(m_verbose){cout << "gpio: " << gpio << endl;}
  if(gpio<0) return false;
  return (((*(m_gpio + m_gpio_to_lev[gpio])) & (1 << gpio)) != 0); 
}

void MultControl::SetMode(uint32_t gpio, uint32_t mode){
  uint8_t fSel = m_gpio_to_sel[gpio];
  uint8_t shift= m_gpio_to_shift[gpio];
 
  if (mode == MODE_INPUT){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) ; // Sets bits to zero = input
  }else if (mode == MODE_OUTPUT){
    *(m_gpio + fSel) = (*(m_gpio + fSel) & ~(7 << shift)) | (1 << shift) ;
  }
}

void MultControl::SetVerbose(bool enable){
  m_verbose=enable;
}

void MultControl::SetEnable(uint8_t EnableSig ){

  if(EnableSig >= 32) {return;} //Enable Sig is  4Bits long.

  for(uint8_t ind=0;ind<4;ind++){
    Write(ENABLES[ind], ( (EnableSig>>ind) & 0x1 ));
  }
}

uint8_t MultControl::GetEnable(){


  uint8_t Val=0;
  for(uint8_t ind=0;ind<4;ind++)
    Val |= ((Read(ENABLES[ind]) & 0x1 ) << ind );

  return Val;

}

