#include "emrb/CS5523.h"
#include <iostream>

static const uint8_t BitMask[9] =
  {0x0,0x1,0x3,0x7,0xF,0x1F,0x3F,0x7F,0xFF};


 


std::vector<uint8_t> CS5523::GetCmd_PerfConv(uint8_t CSRP){

  std::vector<uint8_t> CMD;
  CMD.push_back(0x80);
  CMD.at(0) |= ((CSRP & 0xF)<<3);
  return CMD;
}

std::vector<uint8_t> CS5523::GetCmd_PerfCalib(uint8_t CSRP, uint8_t CCB){

  std::vector<uint8_t> CMD;
  CMD.push_back(0x80);
  CMD.at(0) |= ((CSRP & 0xF)<<3);
  CMD.at(0) |= ((CCB & 0x7));
  return CMD;
}


void CS5523::SetField(const CS5523::Field field, uint32_t data){

  //All fields are maximum 4 bits, so we don't have to worry about them expanding more then one byte

  uint8_t iMask=0;
  uint8_t loc=field.offset;
  uint8_t left=field.size;
  uint8_t inByteSize =0;
  while(left>0){



    
    if  ( ( 8-loc%8) < left )
      inByteSize= (8-loc%8);
    else
      inByteSize= left;
    
    iMask =  ~(BitMask[inByteSize] << (loc%8) );

    field.reg->val[loc/8] &= iMask;
    field.reg->val[loc/8] |= (( (data>>(field.size-left)) & BitMask[inByteSize] ) << (loc%8));  

    // if(field.name=="CH0_GAIN"){
    //   std::cout<<"loc: "<<int(loc)<<" left: "<<int(left)<<" inByteSize: "<<int(inByteSize)<<"Byte:"<<int(loc/8)<<" Val: "<<int(data)<<" Data Left: "<<(data>>(field.size-left))<<std::dec<<" Val at loc: "<<int(field.reg->val[loc/8])<<std::endl;
    // }


    loc+= inByteSize;
    left-=inByteSize;
    
  }

}

uint32_t CS5523::GetField(const CS5523::Field field){

  uint32_t Val=0;
 
  uint8_t loc=field.offset;
  uint8_t left=field.size;
  uint8_t inByteSize =0;
  while(left>0){

    if  ( ( 8-loc%8) < left )
      inByteSize= (8-loc%8);
    else
      inByteSize= left;
    

    Val |= ( (field.reg->val[loc/8]>>(loc%8)) & BitMask[inByteSize] ) << (field.size-left);
  
    // if(field.name=="CH0_GAIN"){
    //   std::cout<<"loc: "<<int(loc)<<" left: "<<int(left)<<" inByteSize: "<<int(inByteSize)<<"Byte:"<<int(loc/8)<<" Val: "<<int(Val)<<std::dec<<" Val at loc: "<<int(field.reg->val[loc/8])<<std::endl;
    // }


    loc+= inByteSize;
    left-=inByteSize;


    
  }

  return Val;
}



std::vector<uint8_t> CS5523::GetCmd_WriteReg(const CS5523::Register reg){

  std::vector<uint8_t> CMD;

  CMD.push_back(0); //Write Command
  
  CMD.at(0) |=  ((reg.RegSet & 0x7)); 

  //RegSet 5 might need a special treatment //TBT
  if(reg.RegSet==5){
    CMD.at(0) |=  ((reg.ChnSet & 0x7)<<4); 
  }
  else{
    CMD.at(0) |=  ((reg.ChnSet & 0x7)<<4); 
  }

  uint8_t SizeToWrite=reg.size;
  //For CRS registers, the writting depth is determined by the DEPTH pointer
  if(reg.RegSet==5){
    SizeToWrite = (GetField(DEPTH_POINTER) +1)*1.5 ;
    //std::cout<<"Depth pointer size: "<<SizeToWrite-1<<std::endl;
  }

  for(uint8_t ind=0; ind<SizeToWrite; ind++){
    CMD.push_back(reg.val[(SizeToWrite-ind-1)]);
  }


  return CMD;

}

std::vector<uint8_t> CS5523::GetCmd_ReadReg(const CS5523::Register reg){

  std::vector<uint8_t> CMD;

  CMD.push_back(0x8); //Write Command
  CMD.at(0) |=  ((reg.RegSet & 0x7)); 
  
  CMD.at(0) |=  ((reg.ChnSet & 0x7)<<4); 

  
  //std::cout<<std::endl<<" Command "<<std::hex<<int(CMD.at(0))<<std::dec<<std::endl;
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  CMD.push_back(0x00);
  return CMD;

}


std::vector<uint8_t> CS5523::GetCmd_SYNC1(){
  return std::vector<uint8_t> {0xF};
}
std::vector<uint8_t> CS5523::GetCmd_SYNC0(){
  return std::vector<uint8_t> {0xE};
}

std::vector<uint8_t> CS5523::GetCmd_NULL(){
  return std::vector<uint8_t> {0x0};
}
