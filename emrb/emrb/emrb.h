#ifndef EMRB_H
#define EMRB_H

#include "emrb/MultControl.h"
#include "emrb/SPIControl.h"
#include <cstdint>

class emrb {
public:
  
  emrb();
  
  ~emrb();
  
  void Init();
  
  uint64_t ReadDateAndTime() const;

  uint8_t SelectChannel(uint32_t chan);
  
  uint32_t ReadRawValue();
  
  double ReadVoltageValue();
  
  double ReadTemperatureValue();

  double CalibrateT(double volts);
  
  double CalibrateV(uint32_t counts);
  
private:
  MultControl * m_EnCon;
  SPIControl * m_SPICon;

  uint32_t m_ADCChannel;
  uint32_t m_MultChannel;
  uint32_t m_channelNumber = static_cast<uint8_t>(-1);

};

#endif
