#ifndef MULTCONTROL_H
#define MULTCONTROL_H

#include <stdint.h>
#include <map>
#include <vector>
#include <mutex>
#include <unistd.h>

/**
 *
 * @brief Multiplex control tool driven from the LED control for Reliance box code.
 * @author ismet.siral@cern.ch
 * @author carlos.solans@cern.ch
 * @author ignacio.asensi@cern.ch
 * @date Feb 2024
 **/

class MultControl {

public:

  const uint32_t ENABLES_PIN[4] = {11,15,16,17}; //pins that are controlling the 4 Enable bits
  const uint32_t ENABLES_GPIO[4] = {17,22,23,24}; //GPIO pins that are controlling the 4 Enable bits

  const uint32_t ENABLES[4] = {17,22,23,24}; //pins that are controlling the 4 Enable bits

  //static const std::vector<uint32_t> ENABLES = {17,22,23,24}; //GPIO pins that are controlling the 4 Enable bits


  
  static const uint32_t MODE_INPUT=0;
  static const uint32_t MODE_OUTPUT=1;
  
  /**
   * Close the communication if any
   **/
  ~MultControl();
  
  /**
   * Get the single instance
   **/
  static MultControl * GetInstance();
 
  /**
   * Get the verbose mode
   * @return True if the verbose mode is enabled
   */
  bool GetVerbose();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if True
   */
  void SetVerbose(bool enable);

  /**
   * Change the state of the LED
   * @param wpi wiringPi number
   * @param enable turn on if true
   **/
  void Write(uint32_t wpi, bool enable);

  /**
   * Change the state of the LED
   * @param wpi wiringPi number
   * @return true if enabled
   **/
  bool Read(uint32_t wpi);

  /**
   * Set the GPIO pins to the 4 Bits sequence 
   * @param 4bit GPIO Signal
   **/
  void SetEnable(uint8_t EnableSig);

  /**
   * Get the GPIO pins to the 4 Bits sequence 
   **/
  uint8_t GetEnable();


 private:

  /**
   * Create an empty MultControl.
   * Private to force users to use the Singleton.
   **/
  MultControl();

  bool Open();

  void Close();

  void SetMode(uint32_t wpi, uint32_t mode);

  uint32_t m_pi_base;

  uint32_t m_gpio_base;

  std::vector<uint8_t> m_gpio_to_set;

  std::vector<uint8_t> m_gpio_to_clr;

  std::vector <uint8_t> m_gpio_to_lev;

  std::vector <uint8_t> m_gpio_to_sel;

  std::vector <uint8_t> m_gpio_to_shift;
  
  uint32_t * m_gpio;

  bool m_verbose;

  int32_t m_fd;
  
  static MultControl * m_me; //!< singleton pointer to be returned by MultControl::GetInstance

  std::mutex m_lock;

};

#endif
